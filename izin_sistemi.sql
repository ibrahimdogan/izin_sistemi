-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 29 Ara 2019, 17:45:21
-- Sunucu sürümü: 10.4.6-MariaDB
-- PHP Sürümü: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `izin_sistemi`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `bolumler`
--

CREATE TABLE `bolumler` (
  `id` int(11) NOT NULL,
  `bolum_adi` varchar(75) COLLATE utf8_turkish_ci NOT NULL,
  `yetkili_adi` varchar(75) COLLATE utf8_turkish_ci NOT NULL,
  `yetkili_email` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `yetkili_sifre` varchar(30) COLLATE utf8_turkish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `bolumler`
--

INSERT INTO `bolumler` (`id`, `bolum_adi`, `yetkili_adi`, `yetkili_email`, `yetkili_sifre`) VALUES
(5, 'HİZMET', 'halit', 'halit@gmail.com', '123456789'),
(6, 'YAZILIM', 'tarık', 'tarik@gmail.com', '123456789'),
(7, 'PAZARLAMA', 'veli', 'veli@gmail.com', '123456789'),
(8, 'ARGE', 'büşra', 'busra@gmail.com', '123456789');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `izinler`
--

CREATE TABLE `izinler` (
  `id` int(11) NOT NULL,
  `il` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `ulke` varchar(100) COLLATE utf8_turkish_ci NOT NULL,
  `adres` varchar(1000) COLLATE utf8_turkish_ci NOT NULL,
  `izin_talep_tarihi` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `izin_baslangic_tarihi` date NOT NULL,
  `izin_bitis_tarihi` date NOT NULL,
  `durumu` int(11) NOT NULL,
  `izin_veren_id` int(11) NOT NULL,
  `izin_alan_id` int(11) NOT NULL,
  `aciklama` varchar(500) COLLATE utf8_turkish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `izinler`
--

INSERT INTO `izinler` (`id`, `il`, `ulke`, `adres`, `izin_talep_tarihi`, `izin_baslangic_tarihi`, `izin_bitis_tarihi`, `durumu`, `izin_veren_id`, `izin_alan_id`, `aciklama`) VALUES
(13, 'diyarbakir', 'Türkiye', 'diyarbakir cinar\r\ndiyarbakir cinar', '2019-12-29 16:29:15', '2019-12-18', '2019-12-20', 0, 2, 6, 'deneme');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `uyeler`
--

CREATE TABLE `uyeler` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `adi_soyadi` varchar(75) COLLATE utf8_turkish_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_turkish_ci NOT NULL,
  `sifre` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `telefon` varchar(12) COLLATE utf8_turkish_ci NOT NULL,
  `bolum_id` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `kota` int(11) NOT NULL DEFAULT 120
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `uyeler`
--

INSERT INTO `uyeler` (`id`, `role_id`, `adi_soyadi`, `email`, `sifre`, `telefon`, `bolum_id`, `kota`) VALUES
(6, 0, 'Hasan Toprak', 'hasan@gmail.com', '123456789', '+90539241714', '2', 120),
(7, 0, 'Arzu Gümüş', 'arzu@gmail.com', '123456789', '+90539241714', '7', 110);

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `bolumler`
--
ALTER TABLE `bolumler`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `izinler`
--
ALTER TABLE `izinler`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `uyeler`
--
ALTER TABLE `uyeler`
  ADD PRIMARY KEY (`id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `bolumler`
--
ALTER TABLE `bolumler`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Tablo için AUTO_INCREMENT değeri `izinler`
--
ALTER TABLE `izinler`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Tablo için AUTO_INCREMENT değeri `uyeler`
--
ALTER TABLE `uyeler`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
