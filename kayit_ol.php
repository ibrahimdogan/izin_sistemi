<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
<form class="form-horizontal" action="" method="POST">
<fieldset>

<!-- Form Name -->
<legend>KAYIT FORMU</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="adi">ADINIZ SOYADINIZ:</label>  
  <div class="col-md-5">
  <input id="adi" name="adi" type="text" placeholder="ADINIZI SOYADINIZI GİRİNİZ..." class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">EMAİL:</label>  
  <div class="col-md-5">
  <input id="email" name="email" type="text" placeholder="EMAİL GİRİNİZ" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="sifre">ŞİFRE:</label>
  <div class="col-md-5">
    <input id="sifre" name="sifre" type="password" placeholder="ŞİFRE GİRİNİZ..." class="form-control input-md" required="">
    
  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="sifre">TEL:</label>
  <div class="col-md-5">
    <input id="sifre" name="telefon" type="text" placeholder="ŞİFRE GİRİNİZ..." class="form-control input-md" required="">
    
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="bolum">DEPARTMAN:</label>
  <div class="col-md-5">
    <select id="bolum" name="bolum" class="form-control">
        <?php
        include 'baglanti.php';
        $query = $db->query("SELECT * FROM bolumler", PDO::FETCH_ASSOC);
        if ( $query->rowCount() ){
             foreach( $query as $row ){  ?>
                  <option value="<?php echo $row['id']; ?>"> <?php echo $row['bolum_adi'] ?> </option>
           <?php  }
        }
        
        ?>
  
    </select>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="kaydet"></label>
  <div class="col-md-4">
    <button id="kaydet" name="kaydet" class="btn btn-success">KAYDET</button>
  </div>
</div>

</fieldset>
</form>

<?php

if(isset($_POST['adi']))
{
    include "baglanti.php";
    $adi=$_POST['adi'];
    $email=$_POST['email'];
    $sifre=$_POST['sifre'];
    $bolum_id=$_POST['bolum'];
    $tel=$_POST['telefon'];

    $query = $db->prepare("INSERT INTO uyeler SET
    adi_soyadi = ?,
    email = ?,
    sifre = ?,
    telefon = ?,
    bolum_id = ?
    ");
    $insert = $query->execute(array(
         $adi,$email,$sifre,$tel,$bolum_id
    ));
    if ( $insert ){
        $last_id = $db->lastInsertId();
        header('location:giris_yap.php');
    }

}


?>
