<?php

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login V2</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="kayit_ol/image/png" href="kayit_ol/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="kayit_ol/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="kayit_ol/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="kayit_ol/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="kayit_ol/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="kayit_ol/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="kayit_ol/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="kayit_ol/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="kayit_ol/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="kayit_ol/css/util.css">
	<link rel="stylesheet" type="text/css" href="kayit_ol/css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter" style="margin-left:40%; margin-top:15%;">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form" action="giris_kontrol.php" method="POST">
					<span class="login100-form-title p-b-26">
						HOŞ GELDİNİZ
					</span>
				
					<span class="login100-form-title p-b-48">
						<i class="zmdi zmdi-font"></i>
					</span>
						<hr>
					<div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
						<label>Email</label><br>
						<input class="input100" type="text" name="email">
						<span class="focus-input100" data-placeholder="Email"></span>
					</div><br>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<label>Şifre</label><br>

						<input class="input100" type="password" name="sifre">
						<span class="focus-input100" data-placeholder="Şifre"></span>
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn">
								Giriş Yap
							</button>
						</div>
					</div>

					<div class="text-center p-t-115">
						<a class="txt2" href="kayit_ol.php">
							Kayıt Ol
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="kayit_ol/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="kayit_ol/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="kayit_ol/vendor/bootstrap/js/popper.js"></script>
	<script src="kayit_ol/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="kayit_ol/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="kayit_ol/vendor/daterangepicker/moment.min.js"></script>
	<script src="kayit_ol/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="kayit_ol/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="kayit_ol/js/main.js"></script>

</body>
</html>

