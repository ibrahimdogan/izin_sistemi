<?php
include 'header.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
</head>
<body>
<hr>
<div class="container">
  <h2>Gelen Görev Talepleri</h2><hr>
  <table class="table">
    <thead>
      <tr>
        <th>Adı Soyadı</th>
        <th>Tel</th>

        <th>İzin Talep Tarihi</th>
        <th>İzin Başlangıç Tarihi</th>
        <th>İzin Bitiş Tarihi </th>
        <th>İzin Durumu </th>
        <th>İzin Gün Sayısı </th>
        <th>Kalan İzin Gün Sayısı</th>
        <th>İşlemler</th>

      </tr>
    </thead>
    <tbody>
        <?php
        include 'baglanti.php';
        $query = $db->query("SELECT *,izinler.id as 'izin_id' FROM uyeler,izinler,bolumler where uyeler.id=izin_alan_id and bolumler.id=izin_veren_id", PDO::FETCH_ASSOC);
        if ( $query->rowCount() ){
             foreach( $query as $row ){
                 
                $diff = abs(strtotime($row['izin_bitis_tarihi']) - strtotime($row['izin_baslangic_tarihi']));
                $yil = floor($diff / (365*60*60*24));
                $ay = floor(($diff - $yil * 365*60*60*24) / (30*60*60*24));
                $gun = floor(($diff - $yil * 365*60*60*24 - $ay*30*60*60*24)/ (60*60*24));
                $durum="Onay Bekleniyor";
                if($row['durumu']=="1")
                {
                    $durum="Onaylandı";
                }
                if($row['durumu']=="2")
                {
                    $durum="Red edildi";
                }
                 
                 ?>
             
                  <tr>
                  <td><?php echo $row['adi_soyadi']; ?></td>
                  <td><?php echo $row['telefon']; ?></td>

                  <td><?php echo $row['izin_talep_tarihi']; ?></td>
                    <td><?php echo $row['izin_baslangic_tarihi']; ?></td>
                    <td><?php echo $row['izin_bitis_tarihi']; ?></td>
                    
                    <td><?php echo $durum ?></td>
                    <td><?php echo $gun; ?></td>
                    <td><?php echo $row['kota']; ?></td>
                    <td>
                    <a href="izin_onayla.php?izin_id=<?php echo $row['izin_id']; ?>"><button>Onayla</button></a>
                    <a href="red_et.php?izin_id=<?php echo $row['izin_id']; ?>"><button style="color:red;">red et</button></a>

                </td>

                </tr>
            <?php }
        }
        
        ?>
   
      
    </tbody>
  </table>
</div>

</body>
</html>
