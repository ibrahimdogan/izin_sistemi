<?php
include 'header.php';
?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

<form class="form-horizontal" action="" method="POST">
<fieldset>
<h3><b style="margin-left:50%">İZİN TALEBİ GÖNDER</b></h3><hr>
<!-- Form Name -->

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="izin_baslangic_suresi">GÖREVİN BAŞLANGIÇ TARİHİ</label>  
  <div class="col-md-5">
  <input id="izin_baslangic_tarihi" name="izin_baslangic_tarihi" type="date" placeholder="GÖREVİN BAŞLANGIÇ SÜRESİ" class="form-control input-md" required="">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="izin_baslangic_suresi">GÖREVİN BİTİŞ TARİHİ</label>  
  <div class="col-md-5">
  <input id="izin_baslangic_tarihi" name="izin_bitis_tarihi" type="date" placeholder="GÖREVİN BAŞLANGIÇ SÜRESİ" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="izin_baslangic_suresi">GÖREVİN YAPILACAĞI İL</label>  
  <div class="col-md-5">
  <input id="izin_baslangic_tarihi" name="il" type="text" placeholder="İL" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="izin_baslangic_suresi">GÖREVİN YAPILACAĞI ÜLKE</label>  
  <div class="col-md-5">
  <input id="izin_baslangic_tarihi" name="ulke" type="text" placeholder="ÜLKE " class="form-control input-md" required="">
    
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="aciklama">GÖREVİN YAPILACAĞI ADRES</label>
  <div class="col-md-5">                     
    <textarea class="form-control" id="adres" name="adres"></textarea>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="aciklama">AÇIKLAMA</label>
  <div class="col-md-5">                     
    <textarea class="form-control" id="aciklama" name="aciklama"></textarea>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="singlebutton"></label>
  <div class="col-md-4">
    <button id="singlebutton" name="singlebutton" class="btn btn-success">İZİN TALEBİ GÖNDER</button>
  </div>
</div>

</fieldset>
</form>



<?php

if(isset($_POST['izin_baslangic_tarihi']))
{
    include "baglanti.php";
    $izin_baslangic=str_replace(".","-",$_POST['izin_baslangic_tarihi']);
    $izin_bitis=str_replace(".","-",$_POST['izin_bitis_tarihi']);
    $aciklama=$_POST['aciklama'];
    $adres=$_POST['adres'];
    $il=$_POST['il'];
    $ulke=$_POST['ulke'];


    $diff = abs(strtotime($izin_bitis) - strtotime($izin_baslangic));
    $yil = floor($diff / (365*60*60*24));
    $ay = floor(($diff - $yil * 365*60*60*24) / (30*60*60*24));
    $gun = floor(($diff - $yil * 365*60*60*24 - $ay*30*60*60*24)/ (60*60*24));

    $query = $db->prepare("INSERT INTO izinler SET
    izin_baslangic_tarihi = ?,
    izin_bitis_tarihi = ?,
    durumu = ?,
    adres=?,
    il=?,
    ulke=?,
    izin_alan_id = ?,
    izin_veren_id= ?,
    aciklama = ?
    ");
    $insert = $query->execute(array(
        $izin_baslangic,$izin_bitis,0,$adres,$il,$ulke,$_SESSION['user_id'],$_SESSION['bolum_id'],$aciklama
    ));
    if ( $insert ){
        $last_id = $db->lastInsertId();
        echo "<h3>izin için onay bekleniyor</h3>";
    }
    else{
        echo "hata";
    }


}

?>